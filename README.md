# challenge-js-calendar

L'objectif de ce challenge sera de réaliser une petite application d'agenda par groupes de trois personnes en 1-2 semaines.
Les contraintes sont les suivantes : 
### Conception
* Réalisation de diagrammes de Use Case en se basant sur les users stories (à voir en dessous)
* Réalisation d'un diagramme de classe
* Réalisation de maquettes (wireframe)
### Technique
* Utilisation de Npm et Webpack
* Utilisation de la librairie moment.js pour la gestion des dates ( https://momentjs.com/ )
* Utilisation d'au moins une classe
* Application responsive (bootstrap)
* Utilisation de git/gitlab pour le travail en groupe

Vous réaliserez également un README avec vos différents diagrammes commentés et une petite présentation du projet.


## User Stories

1. En tant qu'utilisateur.ice, je veux pouvoir visualiser tous mes événements du mois en cours afin de pouvoir m'organiser à court terme
2. En tant qu'utilisateur.ice, je veux pouvoir créer de nouveaux événements afin de ne pas oublier de futurs rendez-vous
3. En tant qu'utilisateur.ice, je veux pouvoir modifier les événements existants afin de pouvoir annuler ou reporter certains d'entre eux
4. Rajouter une user story à vous en vous basant sur les précédentes (elle doit décrire une fonctionnalité que vous voudrez implémenter)

## Les Groupes
1. Jorge, Kalpesh, Aissata, Kamel
2. Benoit, Nidal, Avrora, Dounia
3. Nolan, David, Liridona
4. Louis, Thomas, Melissa
5. Samuel, Jean Jacques, Salwa, Raphael